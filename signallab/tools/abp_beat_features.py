import numpy as np
import pandas as pd
from collections import defaultdict

def pressure_feats(sig, onsets, beatPeriod):
    windex = 40 
    ot = onsets[:-1]
    beatQty = len(ot)

    minDomain = np.zeros((beatQty, windex))
    maxDomain = np.zeros((beatQty, windex)) 

    for i in range(windex):
        minDomain[:,i] = ot-i+1;
        maxDomain[:,i] = ot+i-1;

    minDomain = np.where(minDomain<1, 1, minDomain)
    maxDomain = np.where(maxDomain<1, 1, maxDomain)

    sig[minDomain.astype(int)]
    sig[maxDomain.astype(int)]

    sig_minDomain = sig[minDomain.astype(int)]
    sig_maxDomain = sig[maxDomain.astype(int)]

    # These are 2d indexes, converted to actual index values at times_d/s.
    index_dias = sig_minDomain.argmin(axis=1)
    index_syst = sig_maxDomain.argmax(axis=1)
    
    pressure_dias = sig_minDomain[
                        range(sig_minDomain.shape[0]),
                        index_dias]
    pressure_syst = sig_maxDomain[
                        range(sig_maxDomain.shape[0]),
                        index_syst]

    times_dias = minDomain[
                    range(minDomain.shape[0]),
                    index_dias]
    times_syst = maxDomain[
                    range(maxDomain.shape[0]),
                    index_syst]

    pp = pressure_syst - pressure_dias

    #### Systolic area method 1

    #!! Assumed fs = 125
    # Note I have not changed this to a variable, since there are other sections
    # in wabp that are more difficult to convert to fs != 125, and I don't want to
    # give the false impression that one can simply change a variable.
    beatQty = len(onsets)-1
    ot = onsets[:-1]
    RR           = beatPeriod/125  # RR time in seconds
    sys_duration = 0.3*np.sqrt(RR) # Not really sure why this is sqrt
    endOfSys1    = np.round(ot + sys_duration*125);
    sysArea1     = localfun_area(sig,ot,endOfSys1,pressure_dias);

    #### Systolic area method 2

    slopeWindow = 35
    st = endOfSys1 # start 12 samples after P_sys

    if st[-1] > len(sig)-35:   # error protection
        st[-1] = len(sig)-35
    
    slopeDomain = np.zeros((beatQty,slopeWindow))
    for i in range(slopeWindow):
        slopeDomain[:,i] = st+i-1

    # y[n] = x[n]-x[n-1]
    slope = np.diff(sig[slopeDomain.astype(int)], axis=1)
    slope = np.where(slope>0, 0, slope) # Get rid of positive slopes

    mindex = sig[slope.astype(int)].argmin(axis=1)
    minSlope = sig[slope.astype(int)][mindex]

    endOfSys2 = slopeDomain[
                    range(slopeDomain.shape[0]),
                    mindex]

    sysArea2 = localfun_area(sig,ot,endOfSys2,pressure_dias)

    out = {
        'times_syst':times_syst,
        'delay_syst':times_syst-ot,
        'pressure_syst':pressure_syst,
        'times_dias':times_dias,
        'delay_syst':times_syst-ot,
        'pressure_dias':pressure_dias,
        'pressure_pulse':pp,

        'endOfSys1':endOfSys1,
        'delay_endOfSys1':endOfSys1-ot,
        'sysArea1':sysArea1,

        'endOfSys2':endOfSys2,
        'delay_endOfSys2':endOfSys2-ot,
        'sysArea2':sysArea2,}

    return out

def slope_feats(sig, onsets, beatPeriod):
    dyneg = np.diff(sig)
    dyneg = np.where(dyneg>0, 0, dyneg)
    beatQty = len(onsets)-1

    mean_dyneg = np.zeros(beatQty)

    for i in range(beatQty):
        interval = sig[int(onsets[i]):int(onsets[i+1])]
        dyneg_interval = dyneg[int(onsets[i]):int(onsets[i+1])]
        dyneg_nonzero = np.where(dyneg_interval!=0)[0]
        if len(dyneg_nonzero)==0:
            dyneg_interval = 0
        else:
            dyneg_interval = dyneg_interval[dyneg_nonzero]
        mean_dyneg[i]  = dyneg_interval.mean() if type(dyneg_interval)!=int else 0

    out = {
        'beat_period':beatPeriod,
        'mean_dyneg':mean_dyneg,}

    return out

def smootheness_feats(sig, onsets, beatPeriod):
    beatQty = len(onsets)-1
    smoothness = np.zeros(beatQty)
    mse = np.zeros(beatQty)
    for i in range(beatQty):
        interval = sig[int(onsets[i]):int(onsets[i+1])]
        smoothness[i] = get_smoothness(interval)
        mse[i] = smoothness[i]**-1

    out = {
        'smoothness':smoothness,
        'mse':mse,}

    return out

def heartrate(sig, onsets, beatPeriod):
    hr = 60*125/beatPeriod
    out = {'hr':hr}
    return out

def get_jsqi(sig, onsets, beatPeriod, all_beat_features):
    beatQty = len(onsets)-1
    mapp = np.zeros(beatQty)
    jsqi = np.zeros(beatQty)

    abf = all_beat_features # For shorthand
    for i in range(beatQty):
        interval = sig[int(onsets[i]):int(onsets[i+1])]
        mapp[i] = interval.mean()

    conditions = [
        np.where(abf['pressure_syst']>300, 1, 0),
        np.where(abf['pressure_dias']<20, 1, 0),
        np.where(mapp<30, 1, 0),
        np.where(mapp>200, 1, 0),
        np.where(abf['hr']<20, 1, 0),
        np.where(abf['hr']>200, 1, 0),
        np.where(abf['pressure_pulse']<20, 1, 0),
        np.where(abf['mean_dyneg']<-3, 1, 0),

        np.where(np.pad(np.diff(abf['pressure_syst']),1)[1:]>20,1,0),
        np.where(np.pad(np.diff(abf['pressure_dias']),1)[1:]>20,1,0),
        np.where(np.pad(np.diff(beatPeriod),1)[1:]>2/3,1,0),
        ]
    for ic, con in enumerate(conditions):
        try:
            jsqi = np.bitwise_or(jsqi.astype(int), con.astype(int))
        except TypeError:
            print(ic)
            breakpoint()
    return jsqi

def get_beat_features(sig, onsets, whichfeats=[]):
    functions_dict = {
        'pressure':pressure_feats,
        'slope':slope_feats,
        'smoothness':smootheness_feats,
        'hr':heartrate,
    }

    # This is because jsqi is handled differently, it depends on all of the others.
    jsqi_check=1 if 'jsqi' in whichfeats else 0
    whichfeats = [w for w in whichfeats if w!='jsqi']

    functions = [functions_dict[f] for f in whichfeats if f in functions_dict.keys()]
    #all_beat_features = defaultdict(list)
    all_beat_features = {}

    beatPeriod   = np.diff(onsets)
    for fun in functions:
        some_feats = fun(sig, onsets, beatPeriod)
        all_beat_features.update(some_feats)
        # for sf in some_feats.keys():
        #     #all_beat_features[sf].append(some_feats[sf])
        #     breakpoint()
            

    if jsqi_check:
        all_beat_features['jsqi'] = get_jsqi(sig, onsets, beatPeriod, all_beat_features)

    # beat_features = pd.DataFrame(all_beat_features)
    return all_beat_features

def get_smoothness(arr):
    filt = [1,2,3,2,1]
    assert len(filt)%2==1
    buff = int((len(filt)-1)/2)
    smoothed = np.convolve(arr, filt)[buff:-buff]/sum(filt)
    mse = ((arr-smoothed)**2).sum()/len(arr)
    return mse**-1

def localfun_area(sig,onset,endOfSys,pressure_dias):
    beatQty = len(onset)
    sysArea = np.zeros(beatQty)
    for i in range(beatQty):
        sysArea[i] = sum(  sig[int(onset[i]):int(endOfSys[i])]  )

    sysPeriod = endOfSys-onset
    sysArea = (sysArea - pressure_dias*sysPeriod)/125
    return sysArea