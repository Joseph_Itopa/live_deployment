import numpy as np
from . import tools

def pSQI(signal, fs):
    """
    Calculates the power SQI based on the relative power of the QRS complex over the power of the entire ECG
    """
        
    p_qrs = tools.band_power(signal, fs, 5, 15)
    p_no_qrs = tools.band_power(signal, fs, 5, 40)

    if p_qrs==0:
        return 0.

    p_sqi = p_qrs / p_no_qrs
    return p_sqi
    
def basSQI(signal, fs):
    """
    Calculates the baseline SQI based on the relative power of the baseline over the power of the entire ECG
    """
    
    p_bas = tools.band_power(signal, fs, 0, 1)
    p_sig = tools.band_power(signal, fs, 0, 40)
    
    if p_bas==0:
        return 1.

    bas_sqi = 1 - (p_bas / p_sig)
    return bas_sqi

def snr(signal, fs):
    """
    Calculates the SNR of an ECG signal
    """
    
    p_sig = tools.band_power(signal, fs, 1, 40)
    p_noise = tools.band_power(signal, fs, 0, 1) + tools.band_power(signal, fs, 40, int(fs/2))
    
    if p_sig==0:
        return -16

    snr_log = 10 * np.log10( p_sig / p_noise)
    return snr_log