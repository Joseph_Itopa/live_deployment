from utils import rounddown
import os
from os.path import join as opj
import numpy as np
import pandas as pd
import hjson as json
import datetime
import pytz
import time
from icecream import ic

# Move signallab relevant parts into this repo?
from signallab import SignalLab, tools
from signallab.tools.tools import filtering
from signallab.tools.ecg_qrs_drs import correct_rpeaks
from wfdb.processing.qrs import gqrs_detect as gqrs
from generation import get_ecg, get_lookback, get_shapes, get_cnn, get_rr
from utils import ErrMessage

# General error classes for handling these.
class PatientResponse:
    def __init__(self, message=None):
        self.message = message

class PatientValid(PatientResponse): pass
class AutocalculateOff(PatientValid): pass
class Warmup(PatientValid): pass

class PatientError(PatientResponse): pass
class UnknownError(PatientError): pass

class Patient():
    def __init__(
            self,
            patientID,
            config,
            featuremodels=None, # Might need to rearrange
            timeformat=None,
            ):
        self.patientID = patientID
        self.signals = []
        self.config = config
        self.algorithms = self.config['algorithms']
        if timeformat is None:
            timeformat = r"%m/%d/%Y %H:%M:%S"
        self.timeformat = timeformat

        # This should be the same models object across all patients and 
        # signals. This is created in Handler class, and passed down to here
        # and then to each Signal object.
        # Note that featuremodels are the cnn and shapes models.
        # The actual prediction on these models is done in the Handler class.
        self.featuremodels = featuremodels

    def convert_time(self, attime):
        # Gets the time since 01/01/1970 00:00:00
        nseconds = datetime.datetime.strptime(attime, self.timeformat).timestamp()
        return int(nseconds)
    
    def update_pat(self, jsondata, autocalculate=True):
        assert jsondata['patientID']==self.patientID
        for sig in self.signals:
            if (
                sig.fileID == jsondata['fileID'] and 
                sig.lead == jsondata['lead']
                ):
                signal = sig
                break
        else:
            signal = Signal(
                patientID=jsondata['patientID'],
                fileID=jsondata['fileID'],
                admissionID=jsondata['admissionID'],
                lead=jsondata['lead'],
                fs=jsondata['fs'],
                config=self.config,
                featuremodels=self.featuremodels
                )
            self.signals.append(signal) # Should keep persistent

        time_seconds = self.convert_time(jsondata['time'])
        
        signal.update_sig(newdata=jsondata['data'], attime=time_seconds)
        if not autocalculate:
            return AutocalculateOff()
            
        if (len(signal.data)/signal.fs)/60>10:
            df_all_report = signal.get_features()
            return df_all_report

        else:
            return Warmup()

class Signal():
    def __init__(
            self,
            patientID,
            fileID,
            admissionID,
            lead,
            fs,
            config,
            featuremodels=None # Might need to rearrange
            ):
        self.patientID = patientID
        self.fileID = fileID
        self.admissionID = admissionID
        self.lead = lead
        self.fs = fs
        self.config = config
        self.step_index = int(config['step'] * self.fs)
        self.data  = np.array([])
        self.times = np.array([])
        self.report_times = np.array([])
        self.featuremodels = featuremodels

    def update_sig(self, newdata, attime):


        # attime is the start of the new data, not the end

        # We save the newdata as a variable, since
        # there are situations where we want to use only the 
        # newest data (i.e. with rolling means).
        self.newdata = newdata
        self.newtime = attime
        
        # This is the time of the end of the latest datapoint
        maxnewtime = attime + len(newdata)/self.fs
        start_of_new_times = self.times.max()+(1/self.fs) if len(self.times)>0 else attime
        newtimes = np.arange(start_of_new_times, maxnewtime, 1/self.fs)
        self.times = np.concatenate([
                self.times,
                newtimes,
                ])

        self.data = np.concatenate([
                self.data,
                np.empty(len(newtimes)),
                ])

        self.times = rounddown(self.times, 1/self.fs)
        self.data[np.where(self.times==attime)[0][0]:] = newdata

        # These are the times at which to report new features.
        self.new_report_times = np.arange(
                    self.newtime, 
                    self.times.max(),
                    self.config['step']
                    )
        self.report_times = np.concatenate([
            self.report_times, self.new_report_times
            ])
        if self.config['lockstep']:
            self.report_times = rounddown(self.report_times, self.config['step'])
            self.new_report_times = rounddown(self.new_report_times, self.config['step'])
        else:
            raise NotImplementedError
        
        # Chop times
        self.chop_time_data()

    def chop_time_data(self):
        memtime = self.config.get('memory_time', None)
        if memtime is not None:
            cuttoff_time = self.times.max() - memtime
            cutoff_index = (np.where(self.times<cuttoff_time)[0]).max() if cuttoff_time>self.times.min() else 0
            self.times = self.times[cutoff_index:]
            self.data = self.data[cutoff_index:]

    def get_evaluation_times(self, data, fs):
        pass
    
    def get_features(self):
        data = self.data
        fs   = self.fs
        
        print('Finding peaks')
        peaks = gqrs(data, fs)
        peaks, peak_amps = correct_rpeaks(data, peaks, fs)
            
        evaluation_times = np.arange(int(self.config['step']), int(len(data)/fs), int(self.config['step'])).astype(int)

        # Call the various algorithms
        df_dict_all_algos = {}
        for alg in self.config['algorithms']:
            #TODO ensure params are in the right order. Specify?
            print(alg)

            if alg=='ecg':
                print('\t ecg', time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
                df = get_ecg.process(self.config, data, fs)
                ic()
            
            elif 'ecg_lookback' in alg:
                print('\t ecg_lookback', time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
                lb_number = int(alg[13:]) # After the tag above
                df = get_lookback.process(self.config, df_dict_all_algos['ecg'], lb_number)
                ic()
            
            elif 'shapes' in alg:
                print('\t shapes', time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
                df = get_shapes.process(self.config, data, fs, peaks, evaluation_times, model=self.featuremodels['shapes'][self.lead])
                ic()
            
            elif 'cnn' in alg:
                print('\t cnn', time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
                df = get_cnn.process(self.config, data, fs, peaks, evaluation_times, self.featuremodels['cnn'])
                ic()

            elif 'rr' in alg:
                print('\t rr', time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
                df = get_rr.process(self.config, data, fs, peaks, peak_amps, evaluation_times)
                ic()
            
            else:
                raise NotImplementedError
            
            if type(df)!=ErrMessage and len(df)==0:
                df = ErrMessage('tooshort')
            if type(df)==ErrMessage:
                return df
            
            # Have a think here, ecg reports times at the end of intervals
            # does this match this class?
            if 'lookback' not in alg:
                # The lookback df already inherits the seconds col from ecg.
                newseconds = df['seconds'].values + self.times.min()
                if self.config['lockstep']:
                    newseconds = rounddown(newseconds,self.config['step'])
                else:
                    raise NotImplementedError
                df['seconds'] = newseconds

            # Check this
            df = df[df['seconds'].isin(self.times)]
        
            df_dict_all_algos[alg] = df
    
        df_list_all_algos = [y[1] for y in sorted(
            df_dict_all_algos.items(), key=lambda x: self.config['algorithms'].index(x[0])
            )]            
        
        df_all = df_list_all_algos[0]
        for df_new in df_list_all_algos[1:]:
            df_all = pd.merge(df_all, df_new, how='inner', on='seconds')

        df_all_report = df_all[df_all['seconds'].isin(self.new_report_times)]
        #return df_all_report
        return df_all

def get_new_data(time):
    return {
        'patientID':'NEMC_000',
        'fileID':'0',
        'lead':'II',
        'fs':4.,
        'data':np.random.randn(4*10),
        'time':time
        }
