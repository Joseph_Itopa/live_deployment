import scipy.signal as sps
import scipy.integrate as spi
from scipy import stats
import numpy as np

FTYPES = ['lfilter', 'filtfilt']
FDESIGNS = ['FIR', 'butter', 'cheby1', 'cheby2', 'ellip', 'bessel']
BTYPES = ['lowpass', 'highpass', 'bandpass', 'bandstop']

def filtering(signal, fs, ftype='filtfilt', fdesign='butter', order=3, btype='bandpass', cutoff=None):
    """
    Filters a signal with the specified parameters
    Default values are optimised for ECG processing
    ----- PARAMETERS -----
    signal (numpy array): signal to be filtered
    fs (int): sampling frequency
    ftype (string):
    fdesign (string):
    order (int): filter order (doubled in case of filtfilt)
    btype (string): band type
    cutoff (list): cut-off frequencies
    ----- RETURNS -----
    signal_filt (numpy array): filtered signal
    filter settings (dict): dictionary with filter setting for future check
    """

    # check if input arguments are valid
    if ftype not in FTYPES:
        raise ValueError('filter type not available')
    if fdesign not in FDESIGNS:
        raise ValueError('filter design not available')
    if btype not in BTYPES:
        raise ValueError('band type not available')
    
    # validates sampling frequency as int
    fs = int(fs)

    # define Nyquist frequency
    nyq = 0.5 * fs
    
    # convert cut-off frequency(ies)
    if cutoff is None:
        cutoff = [0.5, 40]
    Wn = np.array(cutoff) / nyq
    
    # calculates filter coefficients
    if fdesign == 'FIR':
        if order % 2 == 0:
            order += 1
        a = np.array([1])
        if btype in ['lowpass', 'bandstop']:
            b = sps.firwin(numtaps=order,
                           cutoff=Wn,
                           pass_zero=True)
        elif btype in ['highpass', 'bandpass']:
            b = sps.firwin(numtaps=order,
                           cutoff=Wn,
                           pass_zero=False)
    elif fdesign == 'butter':
        b, a = sps.butter(N=order,
                          Wn=Wn,
                          btype=btype)
    elif fdesign == 'cheby2':
        b, a = sps.cheby2(N=order,
                          rs=30,
                          Wn=Wn,
                          btype=btype)
    
    # filters the signal
    if ftype == 'filtfilt':
        signal_filt = sps.filtfilt(b, a, signal) 
    elif ftype == 'lfilter':
        signal_filt = sps.lfilter(b, a, signal)
    
    # returns filtered signal and filter settings
    return signal_filt, {'ftype':ftype, 'fdesign':fdesign, 'order':order, 'btype':btype, 'cutoff':cutoff}
    
def power_spectrum(signal, fs, window=None, decibel=False):
    """
    calculates the power spectrum of a signal using fft (one-sided)
    ---- PARAMETERS -----
    signal (numpy array): signal
    fs (int): sampling frequency
    windows (string): apply windows function before fft
    decibel (bool): return power in decibel
    ----- RETURNS -----
    freq (numpy array): frequency vector
    pxx (numpy array): power spectrum
    """
    # fft size same as signal size
    NFFT = len(signal)
    
    # use hamming windows to prevent leakage
    if window is None:
        pass
    elif window in ['hann', 'hanning']:
        h = np.hamming(NFFT)
        signal = signal * h
    else:
        raise ValueError('window not available')
    
    # defines the frequency vector
    freq = np.linspace(0, fs/2, (NFFT//2)+1)
    
    # calculates fft normalize for fft length and conserve energy
    pxx = np.abs( np.fft.fft(signal, NFFT) / NFFT)
    pxx = np.power(pxx, 2)
    pxx[1:] *= 2
    pxx = pxx[:(NFFT//2)+1]
    
    if decibel:
        pxx = 10. * np.log10(pxx)
        
    return freq, pxx

def welch_spectrum(signal, fs, nperseg=None, scaling='density'):
    """
    Calculates the power density spectrum based on Welch's method (one-sided)
    ---- PARAMETERS -----
    signal (numpy array): input signal
    fs (int): sampling frequency (Hz)
    nperseg (int): segment length
    ----- RETURNS -----
    freq (numpy array): frequency vector
    pxx (numpy array): power spectrum
    """
    # sets nperseg to twice the fs as default
    if nperseg is None:
        nperseg = 2 * fs
    
    freq, psd = sps.welch(signal,
                       fs=fs,
                       window='hanning',
                       nperseg=nperseg,
                       detrend='constant',
                       scaling='density')

    return freq, psd

def band_power(signal, fs, low, high, nperseg=None, relative=False):
    """
    calculates the power of a signal in a specific frequency band
    ---- PARAMETERS -----
    signal (numpy array): signal
    fs (int): sampling frequency
    low (float): lower limit of frequencies of interest
    high (float): upper limit of frequencies of interest
    ----- RETURNS -----
    band_power (float): absolute or relative band power
    """
    if low == 0:
        low = 1/fs
        
    # defines default nperseg
    if nperseg is None:
        nperseg = (2/low) * fs

    # calculates the power spectrum density using whelch's method
    freq, psd = welch_spectrum(signal, fs, nperseg=nperseg)

    # finds closest indices of band in frequency vector
    idx = np.logical_and(freq >= low, freq <= high)

    # integral approximation of the spectrum using Simpson's rule
    bp = spi.simps(psd[idx], freq[idx])
    if relative:
        bp /= spi.simps(psd[idx], freq[idx])
        
    return bp
    
def singal_binarization(signal, N, THR=0.2):
    """
    Converts signal to binary signal based on an adaptive threshold
    ---- PARAMETERS -----
    signal (numpy array): input signal
    N (int): window size
    THR (float): threshold value
    ----- RETURNS -----
    binary (numpy array): binary signal
    """
    # pads signal for rolling max
    signal_pad = np.pad(signal, (N//2, N//2), 'reflect')
    
    # performs rolling max calculation
    rl = rolling_max(signal_pad, N)
    
    # smooth rolling max
    rl = rolling_average(rl, 5)

    # converts to binary based on threshold
    binary = np.zeros((len(signal)))
    for i in range(len(signal)):
        if signal[i] > THR * rl[i]:
            binary[i] = 1
    
    return binary

def rolling_average(signal, N):
    """
    Calculates the rolling average of a signal
    ---- PARAMETERS -----
    signal (numpy array): input signal
    N (int): window size
    ----- RETURNS -----
    ra (numpy array): moving average
    """

    ra = np.convolve(signal, np.ones((N,))/N, mode='same')
    
    return ra

def rolling_max(signal, N):
    """
    Calculates the rolling maximum of a signal
    ---- PARAMETERS -----
    signal (numpy array): input signal
    N (int): window size
    ----- RETURNS -----
    rl (numpy array): rolling maximum
    """
        
    rl = np.zeros((len(signal)))
    
    for i in range(len(signal)):
        rl[i] = np.max(signal[i:i+N])
    rl = rolling_average(rl, N)
    
    return rl

def zero_cross():
    """
    Calculates the number of zero cross of a signal
    ----- PARAMETERS -----

    ----- RETURNS -----
    """
    pass
    
def normalise(signal, mean=None, std=None):
    """
    Normalise the signal to zero mean and unit standard deviation and returns
    the values used
    ---- PARAMETERS -----
    signal (numpy array): input signal
    ----- RETURNS -----
    signal_norm (numpy array): normalized signal
    """
    if mean is None and std is None:
        mean = np.mean(signal)
        std = np.std(signal)
    elif mean is not None and std is not None:
        pass
    else:
        raise ValueError('specify both mean and std or none')
    
    signal_normal = signal - mean
    signal_normal /= std
    
    return signal_normal, (mean, std)

def signal_stats(signal, metrics=None):
    """
    Calculates various statistical signal metrics
    ----- PARAMETERS -----
    signal (numpy array): input signal
    ----- RETURNS -----
    mean (float): mean of the signal
    std (float): standard signal deviation (unbiased)
    median (float): median of the signal
    iqr (float): inter quartile range of the signal
    max_amp (float): maximum signal amplitude
    max_abs_amp (float): maximum absolute signal amplitude
    min_amp (float): minimum signal amplitude
    var (float): signal variance (unbiased)
    rms (float): root mean square of the signal
    kurt (float): signal kurtosis (unbiased)
    skew (float): signal skewness (unbiased)
    """
    
    # mean
    mean = np.mean(signal)
    # standard deviation (unbiased)
    std = np.std(signal, ddof=1)
    # median
    median = np.median(signal)
    # inter quartile range
    iqr = stats.iqr(signal)
    # maximum amplitude
    max_amp = np.max(signal)
    # maximum absolute amplitude
    max_abs_amp = np.max( np.abs(signal))
    # minimum amplitude
    min_amp = np.min(signal)
    # variance (unbiased)
    var = np.var(signal, ddof=1)
    # root mean square
    rms = np.sqrt( np.mean( np.square(signal)))
    # kurtosis
    kurt = stats.kurtosis(signal, bias=False)
    # skewness
    skew = stats.skew(signal, bias=False)
    
    return mean, std, median, iqr, max_amp, max_abs_amp, min_amp, var, rms, kurt, skew

def resampling(sig, fs, fs_target, axis=-1):
    """
    Resample the input signal to the desiered frequency
    """
    #t = np.arange(sig.shape[axis]).astype('float64')
    
    new_length = int(sig.shape[axis] * fs_target / fs)
    
    resampled_sig = sps.resample(sig, num=new_length, axis=axis)#,t=t
    
    return resampled_sig#, resampled_t

def nan_interpolate(sig):
    """
    Fills nan values in an numpy array using interpoaltion
    """
    
    nans = np.isnan(sig)
    x = lambda z: z.nonzero()[0]
    
    sig[nans]= np.interp(x(nans), x(~nans), sig[~nans])
    
    return sig