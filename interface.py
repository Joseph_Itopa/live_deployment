import os
from patient import *
from handler import *
import wfdb
import hjson as json
import datetime


def load_all_data(path):
    records = []
    files = os.listdir(path)
    files = [f[:f.index('.dat')] for f in files if '.dat' in f]
    for f in files:
        sig, head = wfdb.rdsamp(os.path.join(path, f))
        fileID = os.path.split(f)[-1]
        newrecord = {
            'patientID': 'SDDB_' + fileID,
            'fileID': fileID,
            'signal': sig[:,0].tolist(),
            'fs': head['fs']
        }
        records.append(newrecord)
    return records

if __name__=='__main__':

    # This is the real-life start time of the script. 
    # It is used 
    script_start_time = datetime.datetime.now().strftime(r"%d-%m-%y-%H-%M-%S")

    with open('live_config.json', 'r') as fopen:
        config = json.load(fopen)

    timeformat = r"%m/%d/%Y %H:%M:%S"
    machine_on_time = datetime.datetime.strptime("07/24/2020 09:00:00", timeformat)

    handler = Handler(config=config)
    records = load_all_data(config['sample_data_path'])

    all_responses = pd.DataFrame()
    
    for i in range(100):

        for rec in records:
            fs = rec['fs']
            sample_length = int(fs*config.get('seconds_in_sample', 10))
            ecg = rec['signal'][i*sample_length:(i+1)*sample_length]
            start_time = machine_on_time + datetime.timedelta(seconds=10*i)
            start_time = datetime.datetime.strftime(start_time, timeformat)

            packet = {
                'patientID': rec['patientID'],
                'fileID': rec['fileID'],
                'admissionID': '',
                'lead': 'II',
                'fs': fs,
                'data': ecg,
                'time': start_time
            }

            handler_response = handler.update_and_predict(packet)
            if issubclass(type(handler_response), PatientValid):
                print(f'Response: {handler_response.message}')
            elif type(handler_response)==ErrMessage:
                print(f'Response: {handler_response.message}')
            else:
                print('Risk:')
                print(handler_response)
                all_responses = pd.concat([all_responses, handler_response])
                all_responses = all_responses.reset_index(drop=1)

        if config.get('save_results', True):
            os.makedirs('saved_results', exist_ok=True)
            filepath = f'saved_results/{script_start_time}'
            all_responses.to_csv(filepath)

    print('done')