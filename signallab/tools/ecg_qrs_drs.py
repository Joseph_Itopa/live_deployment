# -*- coding: utf-8 -*-
"""
Created on Fri Nov  2 18:21:22 2018

@author: Diogo Santos
"""

import numpy as np
import pandas as pd

def detect(sig, fs):
    
    fs = int(fs)
    refract = int(0.2*fs)
    refract_back = int(0.05*fs)
    
    thr = 0.25
    thr_min = 0.1
    thr_frc = 0.3
    
    ts = pd.Series(abs(sig))
    sig_cwt_mean = np.array(ts.rolling(window=fs).mean()[fs-1:])
    sig_cwt_std = np.array(ts.rolling(window=fs).std()[fs-1:])
    sig_cwt_mean = sig_cwt_mean + sig_cwt_std
    del(sig_cwt_std)
    del(ts)
    thr_min = np.concatenate((np.full((len(sig)-len(sig_cwt_mean),1), thr_min)[:,0], sig_cwt_mean))
    del(sig_cwt_mean)
  
    i = 18
    r_peak = []
    length = len(sig)
    sig_cwt_ma_abs = abs(sig)
    
    while i < length:
        if thr <= thr_min[i]:
            thr = thr_min[i]
        else:
            thr = thr*0.999
        if sig[i] > thr:
            idx = np.argmax(sig_cwt_ma_abs[i-refract_back:i+refract])
            r_peak.append(i-refract_back+idx)
            
            thr = thr_frc*np.mean(sig_cwt_ma_abs[r_peak[-4:]])
            
            i = int(i - refract_back + idx + refract)

        else:
            i+=1
            
    return np.array(r_peak)


def correct_rpeaks(signal, rpeaks, fs, tol=0.05):
    
    tol = int(tol * fs)
    length = len(signal)
    rpeak_amp = np.zeros(len(rpeaks))
    
    for i in range(len(rpeaks)):
        a = rpeaks[i] - tol
        if a < 0:
            a = 0
        b = rpeaks[i] + tol
        if b >= length:
            b = length-1
        rpeaks[i] = a + np.argmax( np.abs(signal[a:b]))
        rpeak_amp[i] = signal[rpeaks[i]]
        
    return rpeaks, rpeak_amp