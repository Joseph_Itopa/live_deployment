import numpy as np
import pandas as pd
from scipy import signal as sps

import utils
from utils import ErrMessage

feature_names_all = [f"cnnfeat_{x}" for x in range(8)]

n_points_interval = 64


def process(config, data, fs, peaks, eval_times, model=None):
    #TODO XX
    points_before = int(fs*0.1)
    points_after  = int(fs*0.2)

    intervals, times = utils.get_intervals(
        data,
        fs,
        peaks,
        points_before=points_before,
        points_after=points_after 
        )
    intervals -= intervals.mean(axis=1).reshape(-1,1)

    X = []
    for r in range(config['stack_size']):
        X.append(np.roll(intervals,-r,axis=0))
    
    X = np.stack(X) # shape = stack_size, n_intervals_in_file, n_pts_in_array
    X = np.swapaxes(X, 0, 1) # shape = n_ints_in_file, stack_size, n_pts_in_array
    X = X.reshape([
        X.shape[0],
        X.shape[1],
        X.shape[2],
        1
    ])
    # shape = n_intervals_in_file, stack_size, n_points_in_array, 1, because
    # "colour-like" channels expected

    # np.roll loops around, appending the start to the end also. We therefore
    # need to cut off the data and times at 128 before the end.
    # For the times, we chop from 128 from the beginning onwards, since we want 
    # to count the end of a block as its time.
    X = X[:-(config['stack_size']-1)]
    times = times[(config['stack_size']-1):]

    # Pass the interval stacks through the model a few at a time
    featurevecs_list = []
    batsize = config['cnn_batch_size']
    for i in range(0, len(X), batsize):
        featurevecs_list.append( model.predict(X[i:i+batsize]) )
    
    if len(featurevecs_list)==0: return ErrMessage('tooshort')

    featurevecs = np.concatenate(featurevecs_list, axis=0)

    df = get_meta_vectors_from_array(config, featurevecs, times, eval_times)

    # Mark the nan columns as invalid
    nanidx = utils.find_earliest_nonnan(df, ['feat'], config['stack_size'])
    df['valid_data_cnn'] = 1
    df.iloc[:nanidx]['valid_data_cnn'] = 0

    return df

def get_meta_vectors_from_array(
        config,
        featurevecs,
        times,
        eval_times
        ):
    df = pd.DataFrame({'seconds':eval_times}) 
    columns = [f'cnnfeat_{x:02.0f}' for x in range(featurevecs.shape[1])]
    for i_et,et in enumerate(eval_times):
        eval_indices = np.where((times<et)&(times>et-config['cnn_window']))[0]
        if len(eval_indices)==0:
            continue
        eval_vecs = featurevecs[eval_indices, :]

        df.loc[i_et, columns] = eval_vecs.mean(axis=0)
        
    return df