import numpy as np
import pandas as pd
import math

from signallab import SignalLab
import utils
from utils import ErrMessage

from rr_library import adaf
from rr_library.hjorth_mobility_complexity import hjorth
from rr_library.detrended_fluctuation_analysis import dfa
from rr_library.sampen_v2 import get_sampen
from rr_library.freq_features import process_freq_features

RR_FEATS = ['rrfeat_ectopic_freq',
            'rrfeat_mean',
            'rrfeat_std',
            'rrfeat_std_diff',
            'rrfeat_sd1',
            'rrfeat_sd2',
            'rrfeat_sd12',
            'rrfeat_SE1',
            'rrfeat_SE2',
            'rrfeat_SE3',
            'rrfeat_SE4',
            'rrfeat_VLF',
            'rrfeat_LF',
            'rrfeat_HF',
            'rrfeat_LF_HF',
            'rrfeat_mobility',
            'rrfeat_complexity',
            'rrfeat_alpha',
            'rrfeat_amp_mean',
            'rrfeat_amp_std',
            'rrfeat_amp_median',
            'rrfeat_amp_skew',
            'rrfeat_amp_kurt',
            'rrfeat_beatcorr_mean',
            'rrfeat_beatcorr_std',
            'rrfeat_beatcorr_median',
            'rrfeat_beatcorr_skew',
            'rrfeat_beatcorr_kurt',
            'rrfeat_beatcorr_ectopic_freq']                     
                            
def process(config, data, fs, peaks, peak_amps, evaluation_times):
    rr_np = np.diff(peaks) * 1000/fs
    sig = SignalLab.ECG(data, fs)
    
    # We manually assign these, since they have already been defined in 
    # get_all_offline.
    sig.signal_filt = data
    sig.qrs_idx = peaks
    sig.beat_correlation()
    beat_corr_np = sig.beat_correlations
    del(sig)
    
    wind_len = config['rr_beat_window']

    # Get the three dataframes
    rr = pd.DataFrame(rr_np, columns=['rr'])
    amp = pd.DataFrame(peak_amps, columns=['qrs_amp'])[1:].reset_index()
    beat_corr = pd.DataFrame(beat_corr_np, columns=['beat_corr'])[1:].reset_index()

    # Doing the actual processing
    rr.loc[:, 'rr_adaf'], rr.loc[:, 'X'], _ = adaf.ectopic_cleaning_v2(
                                                rr['rr'].to_numpy())
    rr['rr_adaf'] = rr['rr_adaf'].abs()
    rr['rr_vs_x'] = rr['X'] != rr['rr_adaf']
    rr.loc[:, 'rr_adaf_diff'] = rr['rr_adaf'].diff()
    
    out = pd.DataFrame(columns=RR_FEATS)
    
    cumsum = (rr_np.cumsum()/1000)[wind_len:]
    
    for i, time in enumerate(evaluation_times):
        try:
            closest = np.argmax(cumsum-time>0)
        except:
            return ErrMessage('Not enough peaks')
        
        idx = closest + wind_len
        
        rr_time = round(cumsum[closest], -1)
        
        if cumsum[closest] - time < 2:
            out.loc[i, 'rrfeat_ectopic_freq'] = np.sum(rr['rr_vs_x'][idx-wind_len:idx]) / wind_len
            
            out.loc[i, 'rrfeat_mean'] = rr['rr_adaf'][idx-wind_len:idx].mean()
            out.loc[i, 'rrfeat_std'] = rr['rr_adaf'][idx-wind_len:idx].std()
            out.loc[i, 'rrfeat_std_diff'] = rr['rr_adaf_diff'][idx-wind_len:idx].std()

            res = get_sampen(rr['rr_adaf'][idx-wind_len:idx].to_numpy().copy(), mm=3, r=0.2)
            out.loc[i, 'rrfeat_SE1'] = res[0]
            out.loc[i, 'rrfeat_SE2'] = res[1]
            out.loc[i, 'rrfeat_SE3'] = res[2]
            out.loc[i, 'rrfeat_SE4'] = res[3]

            res = process_freq_features(rr['rr_adaf'][idx-wind_len:idx].to_numpy().copy())
            out.loc[i, 'rrfeat_VLF'] = res[0]
            out.loc[i, 'rrfeat_LF'] = res[1]
            out.loc[i, 'rrfeat_HF'] = res[2]
            out.loc[i, 'rrfeat_LF_HF'] = res[3]

            res = hjorth(rr['rr_adaf'][idx-wind_len:idx].to_numpy().copy())
            out.loc[i, 'rrfeat_mobility'] = res[0]
            out.loc[i, 'rrfeat_complexity'] = res[1]

            out.loc[i, 'rrfeat_alpha'] = dfa(rr['rr_adaf'][idx-wind_len:idx].to_numpy().copy())
            
            out.loc[i, 'rrfeat_amp_mean'] = amp['qrs_amp'][idx-wind_len:idx].mean()
            out.loc[i, 'rrfeat_amp_std'] = amp['qrs_amp'][idx-wind_len:idx].std()
            out.loc[i, 'rrfeat_amp_median'] = amp['qrs_amp'][idx-wind_len:idx].median()
            out.loc[i, 'rrfeat_amp_skew'] = amp['qrs_amp'][idx-wind_len:idx].skew()
            out.loc[i, 'rrfeat_amp_kurt'] = amp['qrs_amp'][idx-wind_len:idx].kurt()
            
            
            out.loc[i, 'rrfeat_beatcorr_mean'] = beat_corr['beat_corr'][idx-wind_len:idx].mean()
            out.loc[i, 'rrfeat_beatcorr_std'] = beat_corr['beat_corr'][idx-wind_len:idx].std()
            out.loc[i, 'rrfeat_beatcorr_median'] = beat_corr['beat_corr'][idx-wind_len:idx].median()
            out.loc[i, 'rrfeat_beatcorr_skew'] = beat_corr['beat_corr'][idx-wind_len:idx].skew()
            out.loc[i, 'rrfeat_beatcorr_kurt'] = beat_corr['beat_corr'][idx-wind_len:idx].kurt()
            out.loc[i, 'rrfeat_beatcorr_ectopic_freq'] = \
                np.sum(beat_corr['beat_corr'][idx-wind_len:idx] > 0.9) / wind_len
    
        else:
            
            for f in RR_FEATS:
                out.loc[i, f] = np.nan

    out['rrfeat_sd1'] = out['rrfeat_std_diff'] / math.sqrt(2.0)
    out['rrfeat_sd2'] = (2.0 * out['rrfeat_std']**2 - 0.5 * out['rrfeat_sd1']**2).pow(1/2)
    out['rrfeat_sd12'] = out['rrfeat_sd1'] / out['rrfeat_sd2']

    out['seconds'] = evaluation_times
    
    """
    # Div 1000 to go ms -> s
    out['seconds'] = (out['rr']/1000).cumsum()
    out['seconds'] = utils.roundup(out['seconds'], config['step'])

    out = out.groupby('seconds').first().reset_index(drop=False)
    out = out[[c for c in out.columns if 'feat' in c or 'seconds' in c]]
    
    # Ensure that seconds = evaluation_times
    extra_seconds = [s for s in evaluation_times if s not in out['seconds'].values]
    out = out.append(pd.DataFrame({'seconds':extra_seconds}))
    out = out[out['seconds'].isin(evaluation_times)]
    out = out.sort_values('seconds')
    
    # Mark the nan columns as invalid
    nanidx = utils.find_earliest_nonnan(out, ['feat'], wind_len)
    out['valid_data_rr'] = 1
    out.iloc[:nanidx]['valid_data_rr'] = 0
    """
    
    return out