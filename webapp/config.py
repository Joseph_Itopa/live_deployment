"""Google Cloud Storage Configuration"""
from os import environ
import os
import json
import logging
from google.cloud import storage
from google.oauth2 import service_account


project_id = 'transformative-deployment'

with open('credientials-gcloud.json') as source:
    info = json.load(source)

storage_credentials = service_account.Credentials.from_service_account_info(info)

storage_client = storage.Client(project=project_id, credentials=storage_credentials)

# Google Cloud Storage
bucketName = environ.get('get-bucket-trans')
bucketFolder = environ.get('test')

# Data
