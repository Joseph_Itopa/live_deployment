import os
import json
import logging

# Imports the Google Cloud client library
from config import storage_client
from flask import Flask, jsonify, request

app = Flask(__name__)

project_id = 'transformative-deployment'

with open('credientials-gcloud.json') as source:
    info = json.load(source)

BUCKET = storage_client.get_bucket("get-bucket-trans") #your bucket name

json_packet = [
                    {
                        "patientID": "ID2342",
                        "fileID": "ID234",
                        "admissionID": "IDe232",
                        "lead": "II",
                        "fs": "250",
                        "data": "2500",
                        "time": "01/02/2020:15:00:00",
                    },
                        {
                            "patientID": "ID2342",
                            "fileID": "ID234",
                            "admissionID": "IDe232",
                            "lead": "II",
                            "fs": "250",
                            "data": "2500",
                            "time": "01/02/2020:15:00:00",
                        }
                ]

@app.route('/', methods = ['GET'])
def add_packet():
    if request.method == 'GET':
        packet = request.get_json()
        json_packet.append(packet)
        return jsonify(json_packet)

@app.route('/')
def home():
    return jsonify(json_packet)

# set the filename of your json object
filename = 'test.json'

def create_json(json_packet, filename):
    '''
    this function will create json object in
    google cloud storage
    '''
    # create a blob
    blob = BUCKET.blob(filename)
    # upload the blob 
    blob.upload_from_string(
        data = json.dumps(json_packet),
        content_type='application/json'
        )
    result = filename + 'upload complete'


# run the function and pass the json_object
print(create_json(json_packet, filename))

if __name__ == '__main__':
  app.run(host = '127.0.0.1', port = 5000, debug = True, threaded = True)