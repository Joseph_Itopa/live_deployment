import os
import pandas as pd
import numpy
import wfdb
import datetime
import hjson as json
from patient import *
import pickle
from tensorflow import keras
from tensorflow.keras.models import Model
from generation.interval_groups import IntervalGroup 

import tensorflow as tf
gpus = tf.config.experimental.list_physical_devices('GPU')
if len(gpus)>0:
    tf.config.experimental.set_memory_growth(gpus[0], True)

class Handler():
    def __init__(self, config):
        self.patients = []
        self.config = config
        self.load_featuremodels()
        self.load_finalmodel()
        self.load_features()
        self.timeformat = r"%m/%d/%Y %H:%M:%S"

    def load_featuremodels(self):
        # This is from gen_all_offline.
        models = {}

        # Load the shapes models
        shapes_models = {}
        with open(self.config['shapes_group_path'], 'rb') as fopen:
            shapes_groups = pickle.load(fopen)
        for g in shapes_groups:
            for lead in g.leads:
                shapes_models[lead] = g
        models['shapes'] = shapes_models

        # Load the CNN models
        cnn_model = keras.models.load_model(self.config['cnn_model_path'])
        if cnn_model.layers[-1].name=='out':
            # If we have the target layer still attached, we want to remove it here.
            cnn_model = Model(
                inputs=cnn_model.input,
                outputs=cnn_model.get_layer('dense_end').output)
        models['cnn'] = cnn_model
            
        self.featuremodels = models
    
    def load_finalmodel(self):

        path = self.config['finalmodel_path']
        finalmodel_type = self.config.get('finalmodel_type', 'keras')
        
        if finalmodel_type=='keras':
            model = keras.models.load_model(path)
        elif finalmodel_type=='xgboost':
            raise NotImplementedError
        else:
            raise NotImplementedError
        
        self.finalmodel = model

    def load_features(self):
        with open(self.config['features_list_path'], 'r') as fopen:
            self.features = fopen.readlines()
        self.features = [f.strip(' ').strip('\n').strip(' ') for f in self.features]

    def get_or_make_patient(self, pid):
        for p in self.patients:
            if p.patientID == pid:
                return p
        else:
            new_patient = Patient(
                patientID=pid,
                config=self.config,
                featuremodels=self.featuremodels,
                timeformat=self.timeformat
                )
            self.patients.append(new_patient)
            return new_patient
    
    def update_and_predict(self, jsondata, mode='pandas'):
        patient_obj = self.get_or_make_patient(jsondata['patientID'])
        patient_response = patient_obj.update_pat(jsondata)

        # This is a way of passing other behavious back up the chain.
        if issubclass(type(patient_response), PatientValid):
            return patient_response
        if type(patient_response)==ErrMessage:
            return patient_response

        unixtimes = patient_response['seconds'].values
        dtobs = [datetime.datetime.fromtimestamp(t) for t in unixtimes]
        dtstrings = [datetime.datetime.strftime(t, self.timeformat) for t in dtobs]

        risks = self.predict(patient_response)
        risks = risks.reshape(-1)

        outdf = pd.DataFrame({
            'patientID':jsondata['patientID'],
            'fileID':jsondata.get('fileID',''),
            'admissionID':jsondata.get('admissionID',''),
            'unixtime':unixtimes,
            'datetime':dtstrings,
            'risk':risks
        })

        if mode=='pandas':
            return outdf
        elif mode=='basic':
            return dtstrings, risks

    def predict(self, patient_response):
        X = patient_response[self.features].values
        X = X.astype(np.float64)
        X = np.nan_to_num(X, 0)
        out = self.finalmodel.predict(X)
        return out